﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WeatherForecastBackend.Models;
using WeatherForecastBackend.Services;

namespace WeatherForecastBackend.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class WeatherController : ControllerBase
    {
        private readonly ILogger<WeatherController> _logger;
        private readonly IOpenWeatherApiService _openWeatherService;

        public WeatherController(ILogger<WeatherController> logger,
            IOpenWeatherApiService openWeatherService)
        {
            _logger = logger;
            _openWeatherService = openWeatherService;
        }

        [HttpPost("api/weather/getlist")]
        public GetWeatherListResponseModel GetList(List<GetWeatherListRequestModel> request)
        {
            try
            {
                return _openWeatherService.GetWeatherList(request);
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}
