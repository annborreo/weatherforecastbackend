﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherForecastBackend.Models
{
    public class GetWeatherListRequestModel
    {
        public string cityName { get; set; }
    }
}
