﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherForecastBackend.Models.OpenWeatherApi;

namespace WeatherForecastBackend.Models
{
    public class GetWeatherInfoResponse
    {
        public main main { get; set; }
        public List<weather> weather { get; set; }
        public string name {get;set; }
        public string degreeSymbol { get; set; }
    }
}
