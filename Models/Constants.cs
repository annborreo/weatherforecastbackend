﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherForecastBackend.Models
{
    public class Constants
    {
        public const string OPENWEATHERAPIURL = "OpenWeatherApiUrl";
        public const string OPENWEATHERAPIAPPID = "OpenWeatherApiAppId";
        public const string OPENWEATHERAPIUNITS = "OpenWeatherApiUnits";
        public const string OPENWEATHERAPIICONURL = "OpenWeatherApiIconUrl";
        public const string DEGREESYMBOL = "DegreeSymbol";
    }
}
