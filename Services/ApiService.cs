using RestSharp;

namespace WeatherForecastBackend.Services
{
    public class ApiService : IApiService
    {
        public ApiService() {
        }

        public object SendJsonRequest(string url, Method method, object body = null) {
            var client = new RestClient(url);
            var request = new RestRequest();
            request.Method = method;
            request.AddHeader("Accept", "application/json");
            if(method == Method.POST || method == Method.PUT)
            {
                request.Parameters.Clear();
                request.RequestFormat = DataFormat.Json;
                request.AddJsonBody(body);
            }

            var response = client.Execute(request);
            var content = response.Content; 
            return content;
        }
    }
}