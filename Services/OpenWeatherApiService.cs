using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using WeatherForecastBackend.Models;

namespace WeatherForecastBackend.Services
{
    public class OpenWeatherApiService : IOpenWeatherApiService
    {
        private readonly IApiService _apiService;
        private readonly IConfiguration _configuration;
        public OpenWeatherApiService(IApiService apiService, IConfiguration configuration) {
            _apiService = apiService;
            _configuration = configuration;
        }
        public GetWeatherListResponseModel GetWeatherList(List<GetWeatherListRequestModel> request) {
            GetWeatherListResponseModel weatherListResponse = new GetWeatherListResponseModel();
            string apiUrl = _configuration[Constants.OPENWEATHERAPIURL];
            string appId = _configuration[Constants.OPENWEATHERAPIAPPID];
            string units = _configuration[Constants.OPENWEATHERAPIUNITS];
            foreach (var item in request)
            {
                string url = string.Format(apiUrl, item.cityName, appId, units);
                string response = (string)_apiService.SendJsonRequest(url, RestSharp.Method.GET);
                var info = JsonSerializer.Deserialize<GetWeatherInfoResponse>(response);
                if(info.weather.Any())
                {
                    info.weather.ForEach(c => c.icon = string.Format(_configuration[Constants.OPENWEATHERAPIICONURL], c.icon));
                }
                info.degreeSymbol = _configuration[Constants.DEGREESYMBOL];
                weatherListResponse.weatherInfo.Add(info);
            }
            return weatherListResponse;
        }
    }
}