using System.Collections.Generic;
using WeatherForecastBackend.Models;

namespace WeatherForecastBackend.Services
{
    public interface IOpenWeatherApiService
    {
        GetWeatherListResponseModel GetWeatherList(List<GetWeatherListRequestModel> request);
    }
}