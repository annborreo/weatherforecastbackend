using RestSharp;

namespace WeatherForecastBackend.Services
{
    public interface IApiService
    {
        object SendJsonRequest(string url, Method method, object body = null);
    }
}